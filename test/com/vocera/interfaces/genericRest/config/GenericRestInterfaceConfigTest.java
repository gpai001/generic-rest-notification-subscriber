/*
 * Copyright (c) Vocera Communications, Inc. All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Vocera Communications, Inc.
 */
package com.vocera.interfaces.genericRest.config;

import com.ext_inc.standards.testing.EITestBase;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * GenericRestInterfaceConfigTest Unit Test class.
 */
public class GenericRestInterfaceConfigTest extends EITestBase {

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of serverName methods, of class GenericRestInterfaceConfig.
     * @throws Exception
     */
    @Test
    public void testServerName() throws Exception {
        System.out.println("serverName");
        GenericRestInterfaceConfig instance = new GenericRestInterfaceConfig();
        instance.setJsonObject(getValidConfig());
        String expResult = "serverName";
        String result = instance.getServerName();
        assertEquals(expResult, result);

        expResult = "newServerName";
        instance.setServerName(expResult);
        result = instance.getServerName();
        assertEquals(expResult, result);
    }

    /**
     * Test of serverIPAddress methods, of class GenericRestInterfaceConfig.
     * @throws Exception
     */
    @Test
    public void testServerIPAddress() throws Exception {
        System.out.println("serverIPAddress");
        GenericRestInterfaceConfig instance = new GenericRestInterfaceConfig(getValidConfig());
        String expResult = "serverIPAddress";
        String result = instance.getServerIPAddress();
        assertEquals(expResult, result);

        expResult = "newServerIPAddress";
        instance.setServerIPAddress(expResult);
        result = instance.getServerIPAddress();
        assertEquals(expResult, result);
    }

    /**
     * Test of serverState methods, of class GenericRestInterfaceConfig.
     * @throws Exception
     */
    @Test
    public void testServerState() throws Exception {
        System.out.println("serverState");
        GenericRestInterfaceConfig instance = new GenericRestInterfaceConfig(getValidConfig());
        String expResult = "serverState";
        String result = instance.getServerState();
        assertEquals(expResult, result);

        expResult = "newServerState";
        instance.setServerState(expResult);
        result = instance.getServerState();
        assertEquals(expResult, result);
    }

    /**
     * Test of serverCity methods, of class GenericRestInterfaceConfig.
     * @throws Exception
     */
    @Test
    public void testServerCity() throws Exception {
        System.out.println("serverCity");
        GenericRestInterfaceConfig instance = new GenericRestInterfaceConfig(getValidConfig());
        String expResult = "serverCity";
        String result = instance.getServerCity();
        assertEquals(expResult, result);

        expResult = "newServerCity";
        instance.setServerCity(expResult);
        result = instance.getServerCity();
        assertEquals(expResult, result);
    }

    /**
     * Test of serverUserName methods, of class GenericRestInterfaceConfig.
     * @throws Exception
     */
    @Test
    public void testServerUserName() throws Exception {
        System.out.println("serverUserName");
        GenericRestInterfaceConfig instance = new GenericRestInterfaceConfig(getValidConfig());
        String expResult = "serverUserName";
        String result = instance.getServerUserName();
        assertEquals(expResult, result);

        expResult = "newServerUserName";
        instance.setServerUserName(expResult);
        result = instance.getServerUserName();
        assertEquals(expResult, result);
    }

    /**
     * Test of serverPassword methods, of class GenericRestInterfaceConfig.
     * @throws Exception
     */
    @Test
    public void testServerPassword() throws Exception {
        System.out.println("serverPassword");
        GenericRestInterfaceConfig instance = new GenericRestInterfaceConfig(getValidConfig());
        String expResult = "serverPassword";
        String result = instance.getServerPassword();
        assertEquals(expResult, result);

        expResult = "newServerPassword";
        instance.setServerPassword(expResult);
        result = instance.getServerPassword();
        assertEquals(expResult, result);
    }

    /**
     * Test of inUse methods, of class GenericRestInterfaceConfig.
     * @throws Exception
     */
    @Test
    public void testInUse() throws Exception {
        System.out.println("inUse");
        GenericRestInterfaceConfig instance = new GenericRestInterfaceConfig(getValidConfig());
        boolean expResult = true;
        boolean result = instance.isInUse();
        assertEquals(expResult, result);

        expResult = false;
        instance.setInUse(expResult);
        result = instance.isInUse();
        assertEquals(expResult, result);
    }

    /**
     * Get a valid config for this object.
     */
    private JSONObject getValidConfig() throws JSONException {
        JSONObject validConfig = new JSONObject();
        validConfig.put("serverName", "serverName");
        validConfig.put("serverIPAddress", "serverIPAddress");
        validConfig.put("serverState", "serverState");
        validConfig.put("serverCity", "serverCity");
        validConfig.put("serverUserName", "serverUserName");
        validConfig.put("serverPassword", "serverPassword");
        validConfig.put("inUse", true);

        return validConfig;
    }
}
