/*
 * Copyright (c) Vocera Communications, Inc. All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Vocera Communications, Inc.
 */
package com.vocera.interfaces.genericRest.application.v1;

import com.ext_inc.standards.framework.EIFramework;
import com.ext_inc.standards.testing.framework.mock.MockEIFramework;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

/**
 * HelloWorldResource Unit Test class.
 */
public class HelloWorldResourceTest {
    /**
     * Test of getMessage method, of class HelloWorldResource.
     */
    @Test
    public void testGetMessage() {
        System.out.println("getMessage");
        HelloWorldResource instance = new HelloWorldResource();
        String expResult = "The framework is currently null.";

        // Framework is null.
        String result = instance.getMessage();
        assertThat(result, containsString(expResult));

        // Framework is not null.
        final String mockComponentName = "MockComponentName";
        MockEIFramework framework = new MockEIFramework() {
            @Override
            public String getInterfaceComponentName() {
                return mockComponentName;
            }
        };
        Whitebox.setInternalState(instance, EIFramework.class, framework);
        expResult = "The interface name is " + mockComponentName + ".";
        result = instance.getMessage();
        assertThat(result, containsString(expResult));
    }
}
