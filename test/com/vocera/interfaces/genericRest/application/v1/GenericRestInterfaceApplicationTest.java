/*
 * Copyright (c) Vocera Communications, Inc. All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Vocera Communications, Inc.
 */
package com.vocera.interfaces.genericRest.application.v1;

import com.ext_inc.standards.testing.EIFrameworkTestBase;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Tests the GenericRestInterfaceApplication
 */
public class GenericRestInterfaceApplicationTest extends EIFrameworkTestBase {

    /**
     * Tests that the method getClasses() returns an empty set (is not overridden).
     */
    @Test
    public void testGetClassesIsEmpty() {
        System.out.println("getClassesIsEmpty");
        GenericRestInterfaceApplication instance = new GenericRestInterfaceApplication();
        assertTrue(instance.getClasses().isEmpty());
    }
}
