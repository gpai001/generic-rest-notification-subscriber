/*
 * Copyright (c) Vocera Communications, Inc. All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Vocera Communications, Inc.
 */
package com.vocera.interfaces.genericRest.osgi;

import com.ext_inc.standards.interfaces.ExtensionInterfaceService;
import com.ext_inc.standards.interfaces.processor.InterfaceProcessorManager;
import com.ext_inc.standards.testing.EIFrameworkTestBase;
import com.vocera.interfaces.genericRest.GenericRestInterface;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.reflect.Whitebox;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.anyString;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.easymock.PowerMock.createMockAndExpectNew;
import static org.powermock.api.easymock.PowerMock.expectLastCall;
import static org.powermock.api.easymock.PowerMock.replayAll;

/**
 * GenericRestActivatorTest Unit Test class.
 *
 * Extend the EIFrameworkTestBase so we don't need to worry about the
 * EIFramework initialization calls that are made.
 */
@PrepareForTest(GenericRestInterface.class)
public class GenericRestActivatorTest extends EIFrameworkTestBase {

    @Before
    public void setUp() throws Exception {
        // Bypass InterfaceProcessorManager creation so we don't have to deal with BundleContext operations
        InterfaceProcessorManager processorManager =
                createMockAndExpectNew(InterfaceProcessorManager.class, anyString());
        expectLastCall().anyTimes();
        processorManager.initializeFramework(anyObject());
        expectLastCall().anyTimes();
        replayAll();
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of doInitialize method, of class GenericRestActivator.
     */
    @Test
    public void testDoInitialize() {
        System.out.println("doInitialize");
        GenericRestActivator instance = new GenericRestActivator();
        instance.doInitialize();

        ExtensionInterfaceService extInterface = Whitebox.getInternalState(instance, "extInterface");
        assertTrue(extInterface instanceof GenericRestInterface);
    }
}
