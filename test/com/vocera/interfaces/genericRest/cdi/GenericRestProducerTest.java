/*
 *  Copyright (c) Vocera Communications, Inc. All Rights Reserved.
 *  This software is the confidential and proprietary information of
 *  Vocera Communications, Inc.
 */
package com.vocera.interfaces.genericRest.cdi;

import com.ext_inc.standards.framework.EIFramework;
import com.ext_inc.standards.interfaces.ExtensionInterfaceService;
import com.ext_inc.standards.interfaces.cdi.Dataset;
import com.ext_inc.standards.interfaces.cdi.InterfaceParam;
import com.ext_inc.standards.interfaces.config.InterfaceSettingsConfig;
import com.ext_inc.standards.testing.EITestBase;
import com.ext_inc.utils.data.executor.DataExecutor;
import com.ext_inc.utils.data.executor.DataExecutorFactory;
import com.ext_inc.utils.logging.EILogger;

import com.vocera.interfaces.genericRest.config.GenericRestInterfaceConfig;

import java.lang.annotation.Annotation;
import javax.enterprise.inject.spi.Annotated;
import javax.enterprise.inject.spi.InjectionPoint;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.annotation.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import static org.easymock.EasyMock.anyString;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;
import static org.powermock.api.easymock.PowerMock.createMock;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replay;
import static org.powermock.api.easymock.PowerMock.replayAll;

/**
 * Tests for the ExtensionInterfaceProducer class
 *
 * @author Bob Mitchell (rmitchell@vocera.com)
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ DataExecutorFactory.class, EILogger.class })
public class GenericRestProducerTest extends EITestBase {
    private GenericRestProducer instance;
    @Mock
    private ExtensionInterfaceService extInterface;
    @Mock
    private InjectionPoint ip;
    @Mock
    private Annotated annotated;
    @Mock
    private EIFramework framework;

    @Before
    public void setUp() throws Exception {
        mockStatic(EILogger.class);
        expect(EILogger.getLogger(anyString())).andReturn(log);
        replay(EILogger.class);
        instance = new GenericRestProducer();
        Whitebox.setInternalState(instance, ExtensionInterfaceService.class, extInterface);
        expect(extInterface.getFramework()).andReturn(framework).anyTimes();
        expect(ip.getAnnotated()).andReturn(annotated).anyTimes();
    }

    @Test
    public void testProduceFramework() {
        System.out.println("produceFramework");
        replayAll();
        EIFramework result = instance.produceFramework();
        assertEquals(framework, result);
    }

    @Test
    public void testProduceInterfaceSettingsConfig() {
        System.out.println("produceInterfaceSettingsConfig");
        InterfaceSettingsConfig isc = new InterfaceSettingsConfig();
        expect(extInterface.getActiveInterfaceSettingsConfig()).andReturn(isc).anyTimes();
        replayAll();
        InterfaceSettingsConfig result = instance.produceInterfaceSettingsConfig();
        assertEquals(isc, result);
    }

    @Test
    public void testProduceInterfaceId() {
        System.out.println("produceInterfaceId");
        InterfaceSettingsConfig isc = new InterfaceSettingsConfig();
        isc.setInterfaceId(14);
        expect(extInterface.getActiveInterfaceSettingsConfig()).andReturn(isc).anyTimes();
        replayAll();
        int result = instance.produceInterfaceId();
        assertEquals(14, result);
    }

    @Test
    public void testProduceInterfaceRefName() {
        System.out.println("produceInterfaceRefName");
        InterfaceSettingsConfig isc = new InterfaceSettingsConfig();
        isc.setInterfaceRefName("Ref Name");
        expect(extInterface.getActiveInterfaceSettingsConfig()).andReturn(isc).anyTimes();
        replayAll();
        String result = instance.produceInterfaceRefName();
        assertEquals("Ref Name", result);
    }


    @Test
    public void testProduceInterfaceConfig() {
        System.out.println("produceInterfaceConfig");
        GenericRestInterfaceConfig config = new GenericRestInterfaceConfig();
        InterfaceSettingsConfig isc = new InterfaceSettingsConfig() {
            @Override
            public GenericRestInterfaceConfig getConfigObject() {
                return config;
            }
        };
        expect(extInterface.getActiveInterfaceSettingsConfig()).andReturn(isc).anyTimes();
        replayAll();
        GenericRestInterfaceConfig result = instance.produceInterfaceConfig();
        assertEquals(config, result);
    }


    @Test
    public void testProduceDataUpdateExecutor() {
        System.out.println("produceDataUpdateExecutor");
        InterfaceSettingsConfig isc = new InterfaceSettingsConfig();
        isc.setInterfaceId(14);
        expect(extInterface.getActiveInterfaceSettingsConfig()).andReturn(isc).anyTimes();
        DataExecutor executor = createMock(DataExecutor.class);
        mockStatic(DataExecutorFactory.class);
        expect(DataExecutorFactory.executorForDatasetName(framework, 14, "SomeDataset")).andReturn(executor);
        expect(annotated.getAnnotation(Dataset.class)).andReturn(new DatasetImpl("SomeDataset")).anyTimes();
        replayAll();
        DataExecutor result = instance.produceDataExecutor(ip);
        assertEquals(executor, result);
    }

    @Test
    public void testProduceLogger() {
        System.out.println("produceLogger");
        replayAll();
        Logger result = instance.produceLogger();
        assertEquals(log, result);
    }

    @Test
    public void testProduceIntegerProperty() {
        System.out.println("produceIntegerProperty");
        expect(framework.getInterfaceParam("property.name")).andReturn("34");
        expect(annotated.getAnnotation(InterfaceParam.class)).andReturn(new InterfaceParamImpl("property.name"))
                .anyTimes();
        replayAll();
        int result = instance.produceIntegerProperty(ip);
        assertEquals(34, result);
    }

    @Test
    public void testProduceIntegerPropertyNoProperty() {
        System.out.println("produceIntegerPropertyNoProperty");
        expect(framework.getInterfaceParam("property.name")).andReturn(null);
        expect(annotated.getAnnotation(InterfaceParam.class)).andReturn(new InterfaceParamImpl("property.name"))
                .anyTimes();
        replayAll();
        int result = instance.produceIntegerProperty(ip);
        assertEquals(0, result);
    }

    @Test
    public void testProduceIntegerPropertyNoPropertyDefault() {
        System.out.println("produceIntegerPropertyNoProperty");
        expect(framework.getInterfaceParam("property.name")).andReturn(null);
        expect(annotated.getAnnotation(InterfaceParam.class)).andReturn(new InterfaceParamImpl("property.name", "23"))
                .anyTimes();
        replayAll();
        int result = instance.produceIntegerProperty(ip);
        assertEquals(23, result);
    }

    @Test
    public void testProduceIntegerPropertyBadProperty() {
        System.out.println("produceIntegerPropertyNoProperty");
        expect(framework.getInterfaceParam("property.name")).andReturn("x");
        expect(annotated.getAnnotation(InterfaceParam.class)).andReturn(new InterfaceParamImpl("property.name"))
                .anyTimes();
        replayAll();
        int result = instance.produceIntegerProperty(ip);
        assertEquals(0, result);
    }

    @Test
    public void testProduceIntegerPropertyBadPropertyDefault() {
        System.out.println("produceIntegerPropertyNoProperty");
        expect(framework.getInterfaceParam("property.name")).andReturn("x");
        expect(annotated.getAnnotation(InterfaceParam.class)).andReturn(new InterfaceParamImpl("property.name", "23"))
                .anyTimes();
        replayAll();
        int result = instance.produceIntegerProperty(ip);
        assertEquals(23, result);
    }

    @Test
    public void testProduceLongProperty() {
        System.out.println("produceLongProperty");
        expect(framework.getInterfaceParam("property.name")).andReturn("34");
        expect(annotated.getAnnotation(InterfaceParam.class)).andReturn(new InterfaceParamImpl("property.name"))
                .anyTimes();
        replayAll();
        long result = instance.produceLongProperty(ip);
        assertEquals(34, result);
    }

    @Test
    public void testProduceLongPropertyNoProperty() {
        System.out.println("produceLongPropertyNoProperty");
        expect(framework.getInterfaceParam("property.name")).andReturn(null);
        expect(annotated.getAnnotation(InterfaceParam.class)).andReturn(new InterfaceParamImpl("property.name"))
                .anyTimes();
        replayAll();
        long result = instance.produceLongProperty(ip);
        assertEquals(0, result);
    }

    @Test
    public void testProduceLongPropertyNoPropertyDefault() {
        System.out.println("produceLongPropertyNoProperty");
        expect(framework.getInterfaceParam("property.name")).andReturn(null);
        expect(annotated.getAnnotation(InterfaceParam.class)).andReturn(new InterfaceParamImpl("property.name", "23"))
                .anyTimes();
        replayAll();
        long result = instance.produceLongProperty(ip);
        assertEquals(23, result);
    }

    @Test
    public void testProduceLongPropertyBadProperty() {
        System.out.println("produceLongPropertyNoProperty");
        expect(framework.getInterfaceParam("property.name")).andReturn("x");
        expect(annotated.getAnnotation(InterfaceParam.class)).andReturn(new InterfaceParamImpl("property.name"))
                .anyTimes();
        replayAll();
        long result = instance.produceLongProperty(ip);
        assertEquals(0, result);
    }

    @Test
    public void testProduceLongPropertyBadPropertyDefault() {
        System.out.println("produceLongPropertyNoProperty");
        expect(framework.getInterfaceParam("property.name")).andReturn("x");
        expect(annotated.getAnnotation(InterfaceParam.class)).andReturn(new InterfaceParamImpl("property.name", "23"))
                .anyTimes();
        replayAll();
        long result = instance.produceLongProperty(ip);
        assertEquals(23, result);
    }

    @Test
    public void testProduceBooleanProperty() {
        System.out.println("produceBooleanProperty");
        expect(framework.getInterfaceParam("property.name")).andReturn("TRUE");
        expect(annotated.getAnnotation(InterfaceParam.class)).andReturn(new InterfaceParamImpl("property.name"))
                .anyTimes();
        replayAll();
        boolean result = instance.produceBooleanProperty(ip);
        assertEquals(true, result);
    }

    @Test
    public void testProduceBooleanPropertyNoProperty() {
        System.out.println("produceBooleanPropertyNoProperty");
        expect(framework.getInterfaceParam("property.name")).andReturn(null);
        expect(annotated.getAnnotation(InterfaceParam.class)).andReturn(new InterfaceParamImpl("property.name"))
                .anyTimes();
        replayAll();
        boolean result = instance.produceBooleanProperty(ip);
        assertEquals(false, result);
    }

    @Test
    public void testProduceBooleanPropertyNoPropertyDefault() {
        System.out.println("produceBooleanPropertyNoProperty");
        expect(framework.getInterfaceParam("property.name")).andReturn(null);
        expect(annotated.getAnnotation(InterfaceParam.class)).andReturn(new InterfaceParamImpl("property.name", "true"))
                .anyTimes();
        replayAll();
        boolean result = instance.produceBooleanProperty(ip);
        assertEquals(true, result);
    }

    @Test
    public void testProduceProperty() {
        System.out.println("produceProperty");
        expect(framework.getInterfaceParam("property.name")).andReturn("something");
        expect(annotated.getAnnotation(InterfaceParam.class))
                .andReturn(new InterfaceParamImpl("property.name", "other")).anyTimes();
        replayAll();
        String result = instance.produceProperty(ip);
        assertEquals("something", result);
    }

    @Test
    public void testProducePropertyNoProperty() {
        System.out.println("producePropertyNoProperty");
        expect(framework.getInterfaceParam("property.name")).andReturn(null);
        expect(annotated.getAnnotation(InterfaceParam.class))
                .andReturn(new InterfaceParamImpl("property.name", "other")).anyTimes();
        replayAll();
        String result = instance.produceProperty(ip);
        assertEquals("other", result);
    }

    private static class InterfaceParamImpl implements InterfaceParam {
        private final String value;
        private final String defaultValue;

        /**
         * Constructor
         * @param value value for value member
         * @param defaultValue value for defaultValue member
         */
        public InterfaceParamImpl(String value, String defaultValue) {
            this.value = value;
            this.defaultValue = defaultValue;
        }

        /**
         * Constructor with no default value
         * @param value value for value member
         */
        public InterfaceParamImpl(String value) {
            this(value, "");
        }

        /* (non-Javadoc)
         * @see java.lang.annotation.Annotation#annotationType()
         */
        @Override
        public Class<? extends Annotation> annotationType() {
            return InterfaceParam.class;
        }

        /* (non-Javadoc)
         * @see com.ext_inc.standards.interfaces.cdi.InterfaceParam#value()
         */
        @Override
        public String value() {
            return value;
        }

        /* (non-Javadoc)
         * @see com.ext_inc.standards.interfaces.cdi.InterfaceParam#defaultValue()
         */
        @Override
        public String defaultValue() {
            return defaultValue;
        }

    }

    private static class DatasetImpl implements Dataset {
        private final String value;

        /**
         * Constructor
         * @param value value for the value member
         */
        public DatasetImpl(String value) {
            this.value = value;
        }

        /* (non-Javadoc)
         * @see java.lang.annotation.Annotation#annotationType()
         */
        @Override
        public Class<? extends Annotation> annotationType() {
            return Dataset.class;
        }

        /* (non-Javadoc)
         * @see com.ext_inc.standards.interfaces.cdi.Dataset#value()
         */
        @Override
        public String value() {
            return value;
        }

    }
}
