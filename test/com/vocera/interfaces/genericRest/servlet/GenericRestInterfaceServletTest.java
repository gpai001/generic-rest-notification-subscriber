/*
 * Copyright (c) Vocera Communications, Inc. All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Vocera Communications, Inc.
 */
package com.vocera.interfaces.genericRest.servlet;

import com.ext_inc.standards.interfaces.ExtensionInterfaceService;
import com.ext_inc.standards.interfaces.processor.InterfaceProcessor;
import com.ext_inc.standards.testing.EIFrameworkTestBase;
import com.vocera.interfaces.genericRest.GenericRestInterface;
import com.vocera.interfaces.genericRest.processor.GenericRestProcessor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.osgi.service.http.HttpService;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.reflect.Whitebox;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.powermock.api.easymock.PowerMock.createNiceMock;

/**
 * GenericRestInterfaceServletTest Unit Test class.
 *
 * This test can also use the grizzly services to test returns.
 * See the 'WebServerTest' in ei-utils-test for more information.
 */
@PrepareForTest({HttpService.class})
public class GenericRestInterfaceServletTest extends EIFrameworkTestBase {

    /**
     * Test of doInitialize method, of class GenericRestInterfaceServlet.
     */
    @Test
    public void testDoInitialize() {
        System.out.println("doInitialize");
        GenericRestInterfaceServlet instance = new GenericRestInterfaceServlet() {
            public Logger getLog() {
                return log;
            }
        };
        instance.doInitialize();
    }

    /**
     * Test of processRequest method, of class GenericRestInterfaceServlet.
     * @throws Exception
     */
    @Test
    public void testProcessRequest() throws Exception {
        System.out.println("processRequest");
        HttpServletRequest req = null;
        HttpServletResponse resp = null;
        GenericRestInterfaceServlet instance = new GenericRestInterfaceServlet();
        instance.processRequest(req, resp);

    }

    /**
     * Test of setExtInterface method, of class GenericRestInterfaceServlet.
     */
    @Test
    public void testSetExtInterface() {
        System.out.println("setExtInterface");
        GenericRestInterface extInterface = new GenericRestInterface() {
            @Override
            public void initializeInterface() {}
        };
        GenericRestInterfaceServlet instance = new GenericRestInterfaceServlet();
        instance.setExtInterface(extInterface);
        ExtensionInterfaceService result = instance.getExtInterface();
        assertEquals(extInterface, result);
    }

    /**
     * Test of unsetExtInterface method, of class GenericRestInterfaceServlet.
     */
    @Test
    public void testUnsetExtInterface() {
        System.out.println("unsetExtInterface");
        GenericRestInterface extInterface = new GenericRestInterface() {
            @Override
            public void initializeInterface() {}
        };
        GenericRestInterfaceServlet instance = new GenericRestInterfaceServlet();
        instance.setExtInterface(extInterface);
        ExtensionInterfaceService result = instance.getExtInterface();
        assertNotNull(result);

        instance.unsetExtInterface(extInterface);
        result = instance.getExtInterface();
        assertNull(result);
    }

    /**
     * Test of Http methods, of class GenericRestInterfaceServlet.
     */
    @Test
    public void testHttp() {
        System.out.println("Http");
        HttpService http = createNiceMock(HttpService.class);
        GenericRestInterfaceServlet instance = new GenericRestInterfaceServlet();
        instance.setHttp(http);
        HttpService result = Whitebox.getInternalState(instance, "http");
        assertEquals(http, result);

        instance.unsetHttp(http);
        result = Whitebox.getInternalState(instance, "http");
        assertNull(result);
    }

    /**
     * Test of getProcessor method, of class GenericRestInterfaceServlet
     */
    @Test
    public void testGetProcessor() {
        System.out.println("getProcessor");
        GenericRestInterfaceServlet instance = new GenericRestInterfaceServlet();
        GenericRestProcessor result = instance.getProcessor(11);
        assertNull(result);
        GenericRestProcessor expected = new GenericRestProcessor();
        GenericRestInterface extInterface = new GenericRestInterface() {
            @Override
            public InterfaceProcessor<?, ?> getProcessor(int interfaceId) {
                return expected;
            }
            @Override
            public void initializeInterface() {}
        };
        instance.setExtInterface(extInterface);
        result = instance.getProcessor(11);
        assertEquals(expected, result);
    }
}
