/*
 * Copyright (c) Vocera Communications, Inc. All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Vocera Communications, Inc.
 */
package com.vocera.interfaces.genericRest.processor;

import com.ext_inc.standards.framework.messaging.EIDataSubscriptionMessage;
import com.ext_inc.standards.framework.messaging.streamhandler.StreamHandlerMessage;
import com.ext_inc.standards.interfaces.config.InterfaceConfig;
import com.ext_inc.standards.interfaces.config.InterfaceSettingsConfig;
import com.ext_inc.standards.testing.EIFrameworkTestBase;
import com.ext_inc.standards.testing.framework.mock.MockEIFramework;
import com.ext_inc.standards.testing.interfaces.config.mock.InterfaceConfigMock;
import com.vocera.interfaces.genericRest.config.GenericRestInterfaceConfig;

import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static com.ext_inc.standards.interfaces.processor.InterfaceProcessor.FAIL_OVER_STARTED;
import static com.ext_inc.standards.interfaces.processor.InterfaceProcessor.FRAMEWORK_PROPERTY;
import static com.ext_inc.standards.interfaces.processor.InterfaceProcessor.INTERFACE_ID_PROPERTY;
import static org.junit.Assert.assertEquals;
import static org.powermock.api.easymock.PowerMock.createMock;

/**
 * GenericRestActivatorTest Unit Test class.
 *
 * Extend the EIFrameworkTestBase so we don't need to worry about the
 * EIFramework initialization calls that are made.
 */
public class GenericRestProcessorTest extends EIFrameworkTestBase {
    private GenericRestProcessor instance;
    private MockEIFramework framework;
    private InterfaceSettingsConfig settingsConfig;
    private GenericRestInterfaceConfig interfaceConfig;

    @Before
    public void setUp() {
        interfaceConfig = new GenericRestInterfaceConfig();
        settingsConfig = new InterfaceSettingsConfig() {
            @Override
            public GenericRestInterfaceConfig getConfigObject() {
                return interfaceConfig;
            }
        };
        instance = new GenericRestProcessor();
        InterfaceConfig config = new InterfaceConfigMock("ExtInt", log) {
            @Override
            public InterfaceSettingsConfig getInterfaceSettingsConfig(int ifid) {
                return settingsConfig;
            }

        };
        framework = new MockEIFramework(log);
        framework.setInterfaceConfig(config);
        Map<String, Object> props = new HashMap<>();
        props.put(FAIL_OVER_STARTED, false);
        props.put(FRAMEWORK_PROPERTY, framework);
        props.put(INTERFACE_ID_PROPERTY, 11);
        instance.activate(props);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of doInitialize method, of class GenericRestActivator.
     */
    @Test
    public void testActivate() {
        System.out.println("activate");
        // Activate called during setup because none of the other methods make sense if it is not called first.
        assertEquals(framework, instance.getFramework());
        assertEquals(interfaceConfig, instance.getInterfaceConfig());
        // TODO add interface specific checks for valid startup.
    }

    @Test
    public void testProcessResponse() {
        System.out.println("processResponse");
        StreamHandlerMessage message = createMock(StreamHandlerMessage.class);
        // TODO Mock out message contents
        instance.processResponse(message);
        // TODO add interface specific tests
    }

    @Test
    public void testProcessDataUpdate() {
        System.out.println("processDataUpdate");
        EIDataSubscriptionMessage message = createMock(EIDataSubscriptionMessage.class);
        // TODO Mock out message contents
        instance.processDataUpdate(message);
        // TODO add interface specific tests
    }

    @Test
    public void testDeactivate() {
        System.out.println("deactivate");
        instance.deactivate();
        // TODO add interface specific tests
    }
}
