/*
 * Copyright (c) Vocera Communications, Inc. All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Vocera Communications, Inc.
 */
package com.vocera.interfaces.genericRest;

import com.ext_inc.standards.interfaces.processor.InterfaceProcessorManager;
import com.ext_inc.standards.testing.EIFrameworkTestBase;

import com.vocera.interfaces.genericRest.config.GenericRestInterfaceConfig;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.powermock.core.classloader.annotations.PrepareForTest;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.anyString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.powermock.api.easymock.PowerMock.createMockAndExpectNew;
import static org.powermock.api.easymock.PowerMock.expectLastCall;
import static org.powermock.api.easymock.PowerMock.replayAll;

/**
 * GenericRestInterfaceTest Unit Test class.
 */
@PrepareForTest(GenericRestInterface.class)
public class GenericRestInterfaceTest extends EIFrameworkTestBase {

    @Before
    public void setUp() throws Exception {
        // Bypass InterfaceProcessorManager creation so we don't have to deal with BundleContext operations
        InterfaceProcessorManager processorManager =
                createMockAndExpectNew(InterfaceProcessorManager.class, anyString());
        expectLastCall().anyTimes();
        processorManager.initializeFramework(anyObject());
        expectLastCall().anyTimes();
        replayAll();
    }

    @After
    public void tearDown() {
    }


    /**
     * Test of defineCustomConfigType method, of class GenericRestInterface.
     */
    @Test
    public void testDefineCustomConfigType() {
        System.out.println("defineCustomConfigType");
        GenericRestInterface instance = new GenericRestInterface();
        Class<GenericRestInterfaceConfig> expResult = GenericRestInterfaceConfig.class;
        Class<GenericRestInterfaceConfig> result = instance.defineCustomConfigType();
        assertEquals(expResult, result);
    }


    /**
     * Test of initializeInterface method, of class GenericRestInterface.
     */
    @Test
    public void testInitializeInterface() {
        System.out.println("initializeInterface");
        GenericRestInterface instance = new GenericRestInterface();
        instance.initializeInterface();
        assertNotNull(instance.getFramework().getQueueMonitor());
        assertNotNull(instance.getFramework().getProcessorManager());
    }
}
