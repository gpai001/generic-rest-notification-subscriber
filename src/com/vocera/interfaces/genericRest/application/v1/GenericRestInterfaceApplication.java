/*
 *  Copyright (c) Vocera Communications, Inc. All Rights Reserved.
 *  This software is the confidential and proprietary information of
 *  Vocera Communications, Inc.
 */
package com.vocera.interfaces.genericRest.application.v1;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Defines the components of a JAX-RS application and supplies additional metadata. The Java Application Programming
 * Interface (API) for REpresentational State Transfer (REST) Services, or JAX-RS, Application for the Extension
 * Interface.
 */
@ApplicationPath("/1")
public class GenericRestInterfaceApplication extends Application {
    // By default we use annotation based discovery
    // Override the getClasses method if you need more control
}
