/*
 *  Copyright (c) Vocera Communications, Inc. All Rights Reserved.
 *  This software is the confidential and proprietary information of
 *  Vocera Communications, Inc.
 */
package com.vocera.interfaces.genericRest.cdi;

import com.ext_inc.standards.framework.EIFramework;
import com.ext_inc.standards.interfaces.ExtensionInterfaceService;
import com.ext_inc.standards.interfaces.cdi.Dataset;
import com.ext_inc.standards.interfaces.cdi.InterfaceIdentifier;
import com.ext_inc.standards.interfaces.cdi.InterfaceParam;
import com.ext_inc.standards.interfaces.config.InterfaceSettingsConfig;
import com.ext_inc.utils.data.executor.DataExecutor;
import com.ext_inc.utils.data.executor.DataExecutorFactory;
import com.ext_inc.utils.logging.EILogger;
import com.vocera.interfaces.genericRest.GenericRestInterface;

import com.vocera.interfaces.genericRest.config.GenericRestInterfaceConfig;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import org.apache.log4j.Logger;
import org.glassfish.osgicdi.OSGiService;

import static com.vocera.interfaces.genericRest.GenericRestInterfaceConstants.INTERFACE_FILTER;

/**
 * Class implementing CDI producers for various objects and values associated with the interface
 * and its active configuration.
 *
 * WARNING: Some of the producers here are @{@link Dependent} scoped which means
 * their values will stay the same across the injection point lifetime.
 * Such values should not be injected into objects whose lifetime is (potentially)
 * greater than the adapter start/stop life-cycle and/or survive over configuration
 * changes (depending on the exact value).  This generally means restricting their
 * use to @{@link RequestScoped} items.
 *
 * The CDI {@link Provider} or {@link Instance} interfaces can be used to work around
 * this limitation.
 *
 * @author Bob Mitchell (rmitchell@vocera.com)
 */
@ApplicationScoped
public class GenericRestProducer {
    @Inject
    @OSGiService(serviceCriteria = INTERFACE_FILTER)
    private ExtensionInterfaceService extInterface;
    private Logger log = EILogger.getLogger(GenericRestInterface.LOGGER_NAME);

    /**
     * @return The interface's configuration object
     */
    @Produces
    @RequestScoped
    EIFramework produceFramework() {
        return extInterface.getFramework();
    }

    /**
     * @return The current interface configuration data
     */
    @Produces
    @RequestScoped
    InterfaceSettingsConfig produceInterfaceSettingsConfig() {
        return extInterface.getActiveInterfaceSettingsConfig();
    }

    /**
     * WARNING: Do not inject this directly in an object whose scope survives interface restarts
     * or configuration updates.
     * @return The id value for the interface's current configuration
     */
    @Produces
    @InterfaceIdentifier
    int produceInterfaceId() {
        return extInterface.getActiveInterfaceSettingsConfig().getInterfaceId();
    }

    /**
     * WARNING: Do not inject this directly in an object whose scope survives interface restarts
     * or configuration updates.
     * @return The reference name for the interface's current configuration
     */
    @Produces
    @InterfaceIdentifier
    String produceInterfaceRefName() {
        return extInterface.getActiveInterfaceSettingsConfig().getInterfaceRefName();
    }


    /**
     * @return The interface's current configuration
     */
    @Produces
    @RequestScoped
    GenericRestInterfaceConfig produceInterfaceConfig() {
        return (GenericRestInterfaceConfig) extInterface.getActiveInterfaceSettingsConfig().getConfigObject();
    }


    /**
     * WARNING: Do not inject this directly in an object whose scope survives interface restarts
     * or configuration updates.
     * @param ip The injection point (to determine the root dataset)
     * @return The data executor
     */
    @Produces
    @Dataset("")
    DataExecutor produceDataExecutor(InjectionPoint ip) {
        Dataset dataset = ip.getAnnotated().getAnnotation(Dataset.class);
        InterfaceSettingsConfig activeSettings = extInterface.getActiveInterfaceSettingsConfig();
        return DataExecutorFactory.executorForDatasetName(extInterface.getFramework(), activeSettings.getInterfaceId(),
                dataset.value());
    }

    /**
     * @return The adapter logger
     */
    @Produces
    @Singleton
    Logger produceLogger() {
        return log;
    }

    /**
     * WARNING: Do not inject this directly in an object whose scope survives interface restarts
     * or configuration updates.
     * @param ip The injection point (for the parameter name and default value)
     * @return The parameter value as in integer, or the default as an integer if it does not exist or is invalid.
     *      Empty default is treated as 0.
     */
    @InterfaceParam("")
    @Produces
    int produceIntegerProperty(InjectionPoint ip) {
        InterfaceParam param = ip.getAnnotated().getAnnotation(InterfaceParam.class);
        String value = extInterface.getFramework().getInterfaceParam(param.value());
        if (value != null) {
            try {
                return Integer.parseInt(value);
            } catch (NumberFormatException e) {
                log.error("Invalid value for " + param.value() + ": " + value);
            }
        }
        return param.defaultValue().isEmpty() ? 0 : Integer.parseInt(param.defaultValue());
    }

    /**
     * WARNING: Do not inject this directly in an object whose scope survives interface restarts
     * or configuration updates.
     * @param ip The injection point (for the parameter name and default value)
     * @return The parameter value as a long, or the default as a long if it does not exist or is invalid.
     *      Empty default is treated as 0.
     */
    @InterfaceParam("")
    @Produces
    long produceLongProperty(InjectionPoint ip) {
        InterfaceParam param = ip.getAnnotated().getAnnotation(InterfaceParam.class);
        String value = extInterface.getFramework().getInterfaceParam(param.value());
        if (value != null) {
            try {
                return Long.parseLong(value);
            } catch (NumberFormatException e) {
                log.error("Invalid value for " + param.value() + ": " + value);
            }
        }
        return param.defaultValue().isEmpty() ? 0 : Long.parseLong(param.defaultValue());
    }

    /**
     * WARNING: Do not inject this directly in an object whose scope survives interface restarts
     * or configuration updates.
     * @param ip The injection point (for the parameter name and default value)
     * @return The parameter value as a boolean, or the default as a boolean if it does not exist.
     */
    @InterfaceParam("")
    @Produces
    boolean produceBooleanProperty(InjectionPoint ip) {
        InterfaceParam param = ip.getAnnotated().getAnnotation(InterfaceParam.class);
        String value = extInterface.getFramework().getInterfaceParam(param.value());
        if (value == null) {
            return Boolean.parseBoolean(param.defaultValue());
        }
        return Boolean.parseBoolean(value);
    }

    /**
     * WARNING: Do not inject this directly in an object whose scope survives interface restarts
     * or configuration updates.
     * @param ip The injection point (for the parameter name and default value)
     * @return The parameter value, or the default if it does not exist.
     */
    @InterfaceParam("")
    @Produces
    String produceProperty(InjectionPoint ip) {
        InterfaceParam param = ip.getAnnotated().getAnnotation(InterfaceParam.class);
        String value = extInterface.getFramework().getInterfaceParam(param.value());
        return value == null ? param.defaultValue() : value;
    }
}
