/*
 * Copyright (c) Vocera Communications, Inc. All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Vocera Communications, Inc.
 */
package com.vocera.interfaces.genericRest.servlet;

import aQute.bnd.annotation.component.Component;
import aQute.bnd.annotation.component.Reference;
import com.ext_inc.standards.interfaces.proxy.InterfaceProxyServlet;
import com.ext_inc.utils.logging.EILogger;
import com.vocera.interfaces.genericRest.GenericRestInterface;
import com.vocera.interfaces.genericRest.processor.GenericRestProcessor;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.osgi.service.http.HttpService;

/**
 * The InterfaceProxyServlet for the generic-rest Interface.
 */
@Component(provide = HttpServlet.class)
public class GenericRestInterfaceServlet extends InterfaceProxyServlet {
    private static final long serialVersionUID = -1L;

    @Override
    public void doInitialize() {
        log = EILogger.getLogger(GenericRestInterface.LOGGER_NAME);
    }

    @Override
    public void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    @Reference
    public void setExtInterface(GenericRestInterface extInterface) {
        super.setExtInterface(extInterface);
    }

    public void unsetExtInterface(GenericRestInterface extInterface) {
        super.unsetExtInterface(extInterface);
    }

    @Override
    @Reference
    protected void setHttp(HttpService http) {
        super.setHttp(http);
    }

    @Override
    protected void unsetHttp(HttpService http) {
        super.unsetHttp(http);
    }

    /**
     * Retrieve the processor for the given interface id if it is available.
     * @param interfaceId The interface id
     * @return the processor for the interface id, or null if not available.
     */
    public GenericRestProcessor getProcessor(int interfaceId) {
        GenericRestInterface extInterface = (GenericRestInterface) getExtInterface();
        return extInterface == null ? null : (GenericRestProcessor) extInterface.getProcessor(interfaceId);
    }
}
