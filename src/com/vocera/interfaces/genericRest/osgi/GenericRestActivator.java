/*
 * Copyright (c) Vocera Communications, Inc. All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Vocera Communications, Inc.
 */
package com.vocera.interfaces.genericRest.osgi;

import com.ext_inc.standards.interfaces.InterfaceActivator;
import com.ext_inc.utils.logging.EILogger;
import com.vocera.interfaces.genericRest.GenericRestInterface;
import org.osgi.framework.BundleActivator;

/**
 * Activator to be used in the OSGi framework.
 *
 */
public class GenericRestActivator extends InterfaceActivator implements BundleActivator {

    /**
     * Method required to initialize all the fields that must
     * be defined for each interface to run.
     *
     * Required fields:
     *   Logger log;
     *   String interfaceServiceName;
     *   String interfaceServicePID;
     *   ExtensionInterface extInterface;
     */
    @Override
    public void doInitialize() {
        log = EILogger.getLogger(GenericRestInterface.LOGGER_NAME);
        interfaceServiceName = GenericRestInterface.class.getName();
        interfaceServicePID = GenericRestInterface.class.getPackage().getName();
        extInterface = new GenericRestInterface();
    }

}
