/*
 *  Copyright (c) Vocera Communications, Inc. All Rights Reserved.
 *  This software is the confidential and proprietary information of
 *  Vocera Communications, Inc.
 */
package com.vocera.interfaces.genericRest.processor;

public class EventStatus {
    private final String message;

    public enum Code {
        SUCCESS,
        FAILURE
    }
    private Code code;

    public EventStatus(Code code) {
        this.code = code;
        message= "";
    }

    public EventStatus(Code code, String message) {
        this.code = code;
        this.message = message;
    }

    public Code getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
