/*
 *  Copyright (c) Vocera Communications, Inc. All Rights Reserved.
 *  This software is the confidential and proprietary information of
 *  Vocera Communications, Inc.
 */
package com.vocera.interfaces.genericRest.processor;

import com.ext_inc.standards.interfaces.processor.InterfaceProcessor;
import org.apache.log4j.Logger;
import org.json.JSONObject;

public abstract class SubscriptionService {

    private final InterfaceProcessor processor;

    protected SubscriptionService(GenericRestProcessor processor) {
        this.processor = processor;
    }

    protected InterfaceProcessor getProcesor() {
        return processor;
    }

    public void onPreSubscription(EventListener listener) {
        listener.onCompleted(new EventStatus(EventStatus.Code.SUCCESS));
    }

    public void onPostSubscription(EventListener listener) {
        listener.onCompleted(new EventStatus(EventStatus.Code.SUCCESS));
    }

    public void onReceiveNotification(JSONObject notification) {
    }

    public Logger getLog() {
        return processor.getLog();
    }

    public abstract JSONObject extractContent(String message);
}
