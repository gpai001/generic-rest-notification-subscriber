/*
 *  Copyright (c) Vocera Communications, Inc. All Rights Reserved.
 *  This software is the confidential and proprietary information of
 *  Vocera Communications, Inc.
 */
package com.vocera.interfaces.genericRest.processor;

import com.ext_inc.utils.logging.EILogger;

import static com.vocera.interfaces.genericRest.GenericRestInterfaceConstants.INTERFACE_NAME;

public class SubscriptionManager {
    private EILogger logger = (EILogger) EILogger.getLogger(INTERFACE_NAME);
    private Subscriber subscriber;

    public SubscriptionManager(GenericRestProcessor processor, SubscriptionService subscriptionService) {
        subscriber = new Subscriber(processor, subscriptionService);
    }

    public boolean startSubscription() {
        return subscriber.startSubscription();
    }

    public boolean stopSubscription() {
        return subscriber.stopSubscription();
    }
}
