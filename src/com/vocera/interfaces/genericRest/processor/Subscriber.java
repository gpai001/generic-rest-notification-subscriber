/*
 *  Copyright (c) Vocera Communications, Inc. All Rights Reserved.
 *  This software is the confidential and proprietary information of
 *  Vocera Communications, Inc.
 */
package com.vocera.interfaces.genericRest.processor;

import com.ext_inc.utils.data.builder.BuilderFactory;
import com.ext_inc.utils.data.builder.DataBuilder;
import com.ext_inc.utils.data.builder.Root;
import com.ext_inc.utils.data.builder.UpdateBuilder;
import com.ext_inc.utils.data.executor.DataExecutor;
import com.ext_inc.utils.data.executor.DataExecutorFactory;
import com.ext_inc.utils.logging.EILogger;
import com.vocera.interfaces.genericRest.config.FieldType;
import com.vocera.interfaces.genericRest.config.NotificationType;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.Optional;
import org.json.JSONObject;

import static com.vocera.interfaces.genericRest.GenericRestInterfaceConstants.INTERFACE_NAME;
import static com.vocera.interfaces.genericRest.processor.EventStatus.Code.SUCCESS;

public class Subscriber {
    private EILogger logger = (EILogger) EILogger.getLogger(INTERFACE_NAME);
    private GenericRestProcessor processor;
    private SubscriptionService subscriptionService;

    public Subscriber(GenericRestProcessor processor, SubscriptionService subscriptionService) {
        this.processor = processor;
        this.subscriptionService = subscriptionService;
    }

    public boolean startSubscription() {
        EventListener listener = new EventListener() {
            @Override
            public void onCompleted(EventStatus status) {
                if (status.getCode() == SUCCESS) {
                    try {
                        subscribe();
                    } catch (URISyntaxException e) {
                        processor.getLog().error("Failed to subscribe:", e);
                    }
                } else {
                    logger.error("failure during pre subscription:" + status.getMessage());
                }
            }
        };
        subscriptionService.onPreSubscription(listener);
        return true;
    }

    private void subscribe() throws URISyntaxException {
        //subscribe
        String urlStr = processor.getInterfaceConfig().getSubscriptionInfo().getSubscriptionUrl();
        URI url = new URI(urlStr);
        processor.getLog().debug("Subscribe(): " + url.toString());

        final WebSocketClientEndpoint clientEndPoint = new WebSocketClientEndpoint(url);

        // add listener
        clientEndPoint.addMessageHandler(message -> {
            processor.getLog().debug("Received message: " + message);
            JSONObject notification = subscriptionService.extractContent(message);
            processor.getLog().debug("Received notification: " + notification);

            Optional<NotificationType> notificationTypeOpt = processor.getInterfaceConfig().getSubscriptionInfo().getNotificationTypes().stream().filter(type -> type.getMatcher().matches(message)).findFirst();

            if(!notificationTypeOpt.isPresent()) {
                 processor.getLog().error("Message:" + message + " Failed to match any notification type and hence using the first notification type as default");
                //TODO:remove this and find the right reg ex to match
                NotificationType notificationType = processor.getInterfaceConfig().getSubscriptionInfo().getNotificationTypes().get(0);
                storeNotification(notificationType, notification);
            } else {
                processor.getLog().debug("Message:" + message + " matched the notification type: " + notificationTypeOpt.get().getReferenceName());
                storeNotification(notificationTypeOpt.get(), notification);
            }
        });

        EventListener listener = new EventListener() {
            @Override
            public void onCompleted(EventStatus status) {
            }
        };
        subscriptionService.onPostSubscription(listener);
    }

    public boolean stopSubscription() {
        return true;
    }

    private void storeNotification(NotificationType notificationType, JSONObject notification) {
        DataExecutor executor = DataExecutorFactory.executor(processor.getFramework(),
                processor.getInterfaceSettings().getInterfaceId(), (int)notificationType.getStartingDataset());
        UpdateBuilder builder = BuilderFactory.updateBuilder();
        Root root = builder.root();
        DataBuilder createOrUpdate = builder.createOrUpdate();

        Iterator<String> keys = notification.keys();

        while(keys.hasNext()) {
            String key = keys.next();
            Optional<FieldType> fieldTypeOpt = notificationType.getFieldTypes().stream().filter(type -> type.getMatcher().matches(key)).findFirst();

            if(fieldTypeOpt.isPresent()) {
                processor.getLog().debug("Field:" + key + " matched the filed type: " + fieldTypeOpt.get().getFieldName());
                if (fieldTypeOpt.get().getFieldValueMatcher().matches((String)notification.get(key))) {
                    JSONObject mappings = new JSONObject();
                    fieldTypeOpt.get().getFieldValueMatcher().putMappedAttributes(mappings);
                    for (String mapKey : JSONObject.getNames(mappings)) {
                        createOrUpdate.set(root.parse(mapKey), mappings.getString(mapKey));
                    }
                } else {
                    processor.getLog().error("Field:" + key + " value did not match field value regex: "
                            + fieldTypeOpt.get().getFieldValueMatcher().getPattern().toString());
                }
            } else {
                processor.getLog().debug("Field:" + key + " did not match any field types");
            }
        }
        processor.getLog().debug("saving(): " + builder.toString());
        executor.update(builder, notificationType.getReferenceName());
    }
}
