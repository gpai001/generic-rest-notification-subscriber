/*
 * Copyright (c) Vocera Communications, Inc. All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Vocera Communications, Inc.
 */
package com.vocera.interfaces.genericRest.processor;

import aQute.bnd.annotation.component.Component;
import com.ext_inc.standards.framework.config.EIConfig;


import com.ext_inc.standards.framework.messaging.EIDataSubscriptionMessage;
import com.ext_inc.standards.framework.messaging.streamhandler.StreamHandlerMessage;
import com.ext_inc.standards.interfaces.processor.InterfaceProcessor;

import com.vocera.interfaces.genericRest.config.tacera.TaceraInterfaceConfig;
import com.vocera.interfaces.genericRest.processor.tacera.TaceraSubscriptionService;
import java.util.Map;

import static com.ext_inc.standards.interfaces.base.ExtensionInterfaceConstants.KEY_INTERFACE_REF_NAME;
import static com.vocera.interfaces.genericRest.GenericRestInterfaceConstants.EVENT_CODE_FAILED_TO_START;
import static com.vocera.interfaces.genericRest.GenericRestInterfaceConstants.EVENT_CODE_FAILED_TO_STOP;
import static com.vocera.interfaces.genericRest.GenericRestInterfaceConstants.PROCESSOR_FACTORY_NAME;

/**
 * The interface processing class.  Each instance handles one interface configuration.  Functionality
 * for a specific interface configuration should be handled by this class.  Specific instances of this
 * class should be requested from the ProcessorManager on the current Framework object.
 */
@Component(factory = PROCESSOR_FACTORY_NAME)
public class GenericRestProcessor extends InterfaceProcessor<TaceraInterfaceConfig, EIConfig> {

    private SubscriptionManager manager;
    private SubscriptionService service;

    @Override
    public void processResponse(StreamHandlerMessage message) {
        // TODO process the response or delete this method if no responses will be requested.
        log.info("Response received: " + message);
    }

    @Override
    public void processDataUpdate(EIDataSubscriptionMessage message) {
        // TODO process the subscription message if applicable or delete this method if subscriptions are not configured
        log.info("Data update received: " + message);
    }

    // DO NOT Delete this method
    @Override
    protected void activate(Map<String, Object> properties) {
        try {
            super.activate(properties);
            service = new TaceraSubscriptionService(this);
            manager = new SubscriptionManager(this, service);
            getLog().info("Processor started for " + getInterfaceSettings().getInterfaceRefName());
            manager.startSubscription();
        } catch (RuntimeException e) {
            log.error("Processor startup failure for " + getInterfaceSettings().getInterfaceRefName(), e);
            getFramework().handleAuditLog(EVENT_CODE_FAILED_TO_START, e,
                    KEY_INTERFACE_REF_NAME, getInterfaceSettings().getInterfaceRefName());
        }
    }

    /**
     * Shut down the processor when the component is destroyed.
     */
    protected void deactivate() {
        try {
            // TODO stop the processor here (e.g. thread shutdown and canceling periodic processing) or delete if not needed
            manager.stopSubscription();
            getLog().info("Processor stopped for " + getInterfaceSettings().getInterfaceRefName());
        } catch (RuntimeException e) {
            log.error("Processor shutdown failure for " + getInterfaceSettings().getInterfaceRefName(), e);
            getFramework().handleAuditLog(EVENT_CODE_FAILED_TO_STOP, e,
                    KEY_INTERFACE_REF_NAME, getInterfaceSettings().getInterfaceRefName());
        }
    }
}
