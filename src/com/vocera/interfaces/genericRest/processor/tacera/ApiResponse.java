/*
 *  Copyright (c) Vocera Communications, Inc. All Rights Reserved.
 *  This software is the confidential and proprietary information of
 *  Vocera Communications, Inc.
 */
package com.vocera.interfaces.genericRest.processor.tacera;

public class ApiResponse {
    private boolean status;
    private String message;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
