/*
 *  Copyright (c) Vocera Communications, Inc. All Rights Reserved.
 *  This software is the confidential and proprietary information of
 *  Vocera Communications, Inc.
 */
package com.vocera.interfaces.genericRest.processor.tacera;

import com.vocera.interfaces.genericRest.processor.GenericRestProcessor;
import com.vocera.interfaces.genericRest.processor.EventListener;
import com.vocera.interfaces.genericRest.processor.EventStatus;
import com.vocera.interfaces.genericRest.processor.SubscriptionService;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import org.json.JSONObject;

public class TaceraSubscriptionService extends SubscriptionService {


    public static String REGISTER = "register";

    public TaceraSubscriptionService(GenericRestProcessor processor) {
        super(processor);
    }

    @Override
    public void onPreSubscription(EventListener listener) {

        AuthenticationResponse authResponse = register(listener);
        subscribe(authResponse.getToken(), listener);
        listener.onCompleted(new EventStatus(EventStatus.Code.SUCCESS));
    }

    @Override
    public JSONObject extractContent(String message) {
        String[] lines = message.split("\n");
        return new JSONObject(lines[lines.length-1]);
    }

    private ApiResponse subscribe(String jwtToken, EventListener listener) {
        String baseUrl = ((GenericRestProcessor)getProcesor()).getInterfaceConfig().getBaseUrl();
        baseUrl += "/alarms/subscribe";
        AlarmSubscription alarmSubscription = new AlarmSubscription();

        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(baseUrl);
        Response response = null;
        ApiResponse apiResponse = null;
        try {
            getLog().debug ("Trying to register");
            response = target.request(MediaType.APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION, "Bearer " + jwtToken)
                    .put(Entity.json(alarmSubscription));
            getLog().debug ("\nresponse from Tacera: " + response);
            if (response.getStatus() != 200) {
                getLog().error("onPreSubscription: failed to publish \n"
                        + "\nresponse from Tacera: " + response);
                listener.onCompleted(new EventStatus(EventStatus.Code.FAILURE, "Failed to register"));
            }

            apiResponse = response.readEntity(ApiResponse.class);
            getLog().debug ("Message" + apiResponse.getMessage());

        } finally {
            response.close();
        }
        return apiResponse;
    }
    
    private AuthenticationResponse register(EventListener listener) {
        //Register
        String baseUrl = ((GenericRestProcessor)getProcesor()).getInterfaceConfig().getBaseUrl();
        baseUrl += "/" + REGISTER;

        JSONObject registration = new JSONObject();
        registration.put("featureName", "WS-ALARM-SUBSCRIBE");

        JSONObject registrationInfo = new JSONObject();
        registrationInfo.put("apiKey", ((GenericRestProcessor)getProcesor()).getInterfaceConfig().getApiKey());
        registrationInfo.put("registrations", new JSONArray().put(registration));

        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(baseUrl);

        Response response = null;
        AuthenticationResponse authenticationResponse = null;
        try {
            getLog().debug ("Trying to register");
            response = target.request(MediaType.APPLICATION_JSON)
                    .put(Entity.json(registrationInfo));
            getLog().debug ("\nresponse from Tacera: " + response);
            if (response.getStatus() != 200) {
                getLog().error("onPreSubscription: failed to publish \n"
                        + "\nresponse from Tacera: " + response);
                listener.onCompleted(new EventStatus(EventStatus.Code.FAILURE, "Failed to register"));
            }

            authenticationResponse = response.readEntity(
                    AuthenticationResponse.class);
            getLog().debug ("JWT token" + authenticationResponse.getToken());

        } finally {
            response.close();
        }
        return authenticationResponse;
    }
}
