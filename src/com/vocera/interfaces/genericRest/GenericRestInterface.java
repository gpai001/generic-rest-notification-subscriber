/*
 * Copyright (c) Vocera Communications, Inc. All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Vocera Communications, Inc.
 */
package com.vocera.interfaces.genericRest;

import com.ext_inc.standards.interfaces.ExtensionInterface;
import com.ext_inc.standards.interfaces.InterfaceQueueMonitor;
import com.ext_inc.standards.interfaces.processor.InterfaceProcessorManager;
import com.vocera.interfaces.genericRest.config.tacera.TaceraInterfaceConfig;

import static com.vocera.interfaces.genericRest.GenericRestInterfaceConstants.PROCESSOR_FACTORY_NAME;

/**
 * generic-rest Interface that can be run in the Extension OSGi environment.
 */
public class GenericRestInterface extends ExtensionInterface {

    /**
     * Set the logger name so it can be shared among the components.
     */
    public static final String LOGGER_NAME = GenericRestInterface.class.getSimpleName();

    /**
     * Constructor. Calls super class with the name of the log file
     * to create for this interface instance.
     */
    public GenericRestInterface() {
        super(LOGGER_NAME);
    }


    @Override
    public Class<TaceraInterfaceConfig> defineCustomConfigType() {
        return TaceraInterfaceConfig.class;
    }


    @Override
    public void initializeInterface() {
        // Set the queueMonitor and processManager in the framework.
        getFramework().setQueueMonitor(new InterfaceQueueMonitor());
        getFramework().setProcessorManager(new InterfaceProcessorManager(PROCESSOR_FACTORY_NAME));
    }
}
