/*
 * Copyright (c) Vocera Communications, Inc. All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Vocera Communications, Inc.
 */
package com.vocera.interfaces.genericRest;

import com.ext_inc.standards.interfaces.base.ExtensionInterfaceConstants;
import org.osgi.framework.Constants;

/**
 * Constants used in the generic-rest Interface.
 */
public interface GenericRestInterfaceConstants extends ExtensionInterfaceConstants {

    /**
     * Event code for failure to start a processor.
     * <p>
     * Statement: Failed to start processing for {interfaceRefName} interface configuration
     */
    int EVENT_CODE_FAILED_TO_START = 613;

    /**
     * Event code for failure to stop a processor.
     * <p>
     * Statement: Failed to stop processing for {interfaceRefName} interface configuration
     */
    int EVENT_CODE_FAILED_TO_STOP = 612;

    /**
     * Factory name for the processor component.
     */
    String PROCESSOR_FACTORY_NAME = "com.vocera.interfaces.genericRest.processor.TaceraRestProcessor";

    /**
     * The OSGi service filter to use to find this service's interface object.
     */
    String INTERFACE_FILTER = "(" + Constants.SERVICE_PID + "=com.vocera.interfaces.genericRest)";

    String INTERFACE_NAME = "GenericRest";
}
