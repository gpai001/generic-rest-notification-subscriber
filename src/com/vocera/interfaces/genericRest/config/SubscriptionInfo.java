/*
 *  Copyright (c) Vocera Communications, Inc. All Rights Reserved.
 *  This software is the confidential and proprietary information of
 *  Vocera Communications, Inc.
 */
package com.vocera.interfaces.genericRest.config;

import com.ext_inc.standards.exceptions.EIMissingFieldsException;
import com.ext_inc.standards.framework.annotations.EIProperty;
import com.ext_inc.standards.framework.config.EIConfig;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class SubscriptionInfo extends EIConfig {
    private String subscriptionUrl;
    private long heartbeatInterval;
    private List<NotificationType> notificationTypes;

    public SubscriptionInfo() {
        super();
    }

    public SubscriptionInfo(JSONObject subscriptionInfo) throws EIMissingFieldsException {
        super(subscriptionInfo);
    }

    public String getSubscriptionUrl() {
        return subscriptionUrl;
    }

    @EIProperty(name="subscriptionUrl", required=true)
    public void setSubscriptionUrl(String subscriptionUrl) {
        this.subscriptionUrl = subscriptionUrl;
    }

    public long getHeartbeatInterval() {
        return heartbeatInterval;
    }

    @EIProperty(name="heartbeatInterval", required=true)
    public void setHeartbeatInterval(long timeout) {
        this.heartbeatInterval = timeout;
    }

    public List<NotificationType> getNotificationTypes() {
        return notificationTypes;
    }

    @EIProperty(name="notificationTypes", required=true)
    public void setNotificationTypes(JSONArray notificationTypes) {
        this.notificationTypes = new ArrayList<>();
        for (int i = 0; i < notificationTypes.length(); i++) {
            try {
                NotificationType notificationType = new NotificationType(notificationTypes.getJSONObject(i));
                this.notificationTypes.add(notificationType);
            } catch (EIMissingFieldsException e) {
                addMissingField(String.format("notificationTypes[%d]: %s", i, e.getMissingFields()));
            }
        }
    }
}
