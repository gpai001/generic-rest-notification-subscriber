/*
 *  Copyright (c) Vocera Communications, Inc. All Rights Reserved.
 *  This software is the confidential and proprietary information of
 *  Vocera Communications, Inc.
 */
package com.vocera.interfaces.genericRest.config;

import com.ext_inc.standards.exceptions.EIMissingFieldsException;
import com.ext_inc.standards.framework.annotations.EIProperty;
import com.ext_inc.standards.framework.config.EIConfig;
import com.ext_inc.utils.EIMatcher;
import com.ext_inc.utils.JSONUtils;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;

public class FieldType extends EIConfig {
    private EIMatcher matcher;
    private EIMatcher fieldValueMatcher;
    private String fieldName;
    /**
     * Base config constructor.
     */
    public FieldType(JSONObject subscriptionInfo) throws EIMissingFieldsException {
        super(subscriptionInfo);
    }

    public EIMatcher getMatcher() {
        if (matcher == null) {
            matcher = new EIMatcher();
        }
        return matcher;
    }

    public EIMatcher getFieldValueMatcher() {
        if (fieldValueMatcher == null) {
            fieldValueMatcher = new EIMatcher();
        }
        return fieldValueMatcher;
    }

    @EIProperty(name = "fieldRegex", required = true)
    public void setRegex(String regex) {
        getMatcher().setPattern(Pattern.compile(regex));
    }

    @EIProperty(name = "fieldValueRegex", required = true)
    public void setFileValueRegex(String fieldValueRegex) {
        getFieldValueMatcher().setPattern(Pattern.compile(fieldValueRegex));
    }

    @EIProperty(name = "fieldMapping")
    public void setFieldMapping(JSONArray regexMapping) {
        getFieldValueMatcher().setMapping(JSONUtils.jsonArrayToStringList(regexMapping));
    }

    public String getFieldName() {
        return fieldName;
    }

    @EIProperty(name = "fieldName")
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

}
