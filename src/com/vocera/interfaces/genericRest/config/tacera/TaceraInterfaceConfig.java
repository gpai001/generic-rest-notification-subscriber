/*
 *  Copyright (c) Vocera Communications, Inc. All Rights Reserved.
 *  This software is the confidential and proprietary information of
 *  Vocera Communications, Inc.
 */
package com.vocera.interfaces.genericRest.config.tacera;

import com.ext_inc.standards.exceptions.EIMissingFieldsException;
import com.ext_inc.standards.framework.annotations.EIProperty;
import com.vocera.interfaces.genericRest.config.GenericRestInterfaceConfig;
import org.json.JSONObject;

public class TaceraInterfaceConfig extends GenericRestInterfaceConfig {
    private String baseUrl ;
    private String apiKey;

    /**
     * Base config constructor.
     */
    public TaceraInterfaceConfig() {
        super();
    }

    /**
     * Config with the JSONObject to be used.
     *
     * @param jobj The original configuration object.
     * @throws EIMissingFieldsException If there are required fields missing.
     */
    public TaceraInterfaceConfig(JSONObject jobj) throws EIMissingFieldsException {
        super(jobj);
    }
    public String getBaseUrl() {
        return baseUrl;
    }

    @EIProperty(name="baseUrl")
    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getApiKey() {
        return apiKey;
    }

    @EIProperty(name="apiKey")
    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}
