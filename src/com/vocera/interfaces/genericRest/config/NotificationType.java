/*
 *  Copyright (c) Vocera Communications, Inc. All Rights Reserved.
 *  This software is the confidential and proprietary information of
 *  Vocera Communications, Inc.
 */
package com.vocera.interfaces.genericRest.config;

import com.ext_inc.standards.exceptions.EIMissingFieldsException;
import com.ext_inc.standards.framework.annotations.EIProperty;
import com.ext_inc.standards.framework.config.EIConfig;
import com.ext_inc.utils.EIMatcher;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;

public class NotificationType extends EIConfig {

    private String referenceName;
    List<FieldType> fieldTypes;
    private EIMatcher matcher;
    private JSONObject datasets;
    private long startingDataset;

    public NotificationType() {
        super();
    }

    /**
     * Base config constructor.
     */
    public NotificationType(JSONObject notificationType) throws EIMissingFieldsException {
        super(notificationType);
    }

    public String getReferenceName() {
        return referenceName;
    }


    @EIProperty(name = "referenceName", required = true)
    public void setReferenceName(String referenceName) {
        this.referenceName = referenceName;
    }

    public List<FieldType> getFieldTypes() {
        return fieldTypes;
    }

    @EIProperty(name="fieldTypes", required=true)
    public void setFieldTypes(JSONArray fieldTypes) {
        this.fieldTypes = new ArrayList<>();
        for (int i = 0; i < fieldTypes.length(); i++) {
            try {
                FieldType notificationType = new FieldType(fieldTypes.getJSONObject(i));
                this.fieldTypes.add(notificationType);
            } catch (EIMissingFieldsException e) {
                addMissingField(String.format("fieldTypes[%d]: %s", i, e.getMissingFields()));
            }
        }
    }

    public EIMatcher getMatcher() {
        if (matcher == null) {
            matcher = new EIMatcher();
        }
        return matcher;
    }

    @EIProperty(name = "notificationRegex", required = true)
    public void setNotificationRegex(String notificationRegex) {
        getMatcher().setPattern(Pattern.compile(notificationRegex));
    }

    @EIProperty(name = "datasets", required = false)
    public void setDatasets(JSONObject datasets) {
        this.datasets = datasets;
        if (!datasets.has("startingDataset")) {
            this.addMissingField("startingDataset");
        }
        this.startingDataset = datasets.optInt("startingDataset", 0);
    }

    public long getStartingDataset() {
        return startingDataset;
    }
}
