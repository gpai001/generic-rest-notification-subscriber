/*
 * Copyright (c) Vocera Communications, Inc. All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Vocera Communications, Inc.
 */
package com.vocera.interfaces.genericRest.config;

import com.ext_inc.standards.exceptions.EIMissingFieldsException;
import com.ext_inc.standards.framework.annotations.EIProperty;
import com.ext_inc.standards.framework.config.EIConfig;
import org.json.JSONObject;

/**
 * Configuration object for storing data from the interfaceSettings for
 * this interface.
 */
public class GenericRestInterfaceConfig extends EIConfig {
    private SubscriptionInfo subscriptionInfo;

    /**
     * Base config constructor.
     */
    public GenericRestInterfaceConfig() {
        super();
    }

    /**
     * Config with the JSONObject to be used.
     *
     * @param jobj The original configuration object.
     * @throws EIMissingFieldsException If there are required fields missing.
     */
    public GenericRestInterfaceConfig(JSONObject jobj) throws EIMissingFieldsException {
        super(jobj);
    }

    public SubscriptionInfo getSubscriptionInfo() {
        return subscriptionInfo;
    }

    @EIProperty(name="subscriptionInfo")
    public void setSubscriptionInfo(JSONObject subscriptionInfo) throws EIMissingFieldsException {
        try {
            this.subscriptionInfo = new SubscriptionInfo(subscriptionInfo);
        } catch (EIMissingFieldsException e) {
            this.subscriptionInfo = new SubscriptionInfo();
        }
    }
}
