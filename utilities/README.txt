This directory is for tools that you may find useful in working with your adapter.

The exampleConfigs directory contains configurations that you can use as a starting point
for testing and especially for unit tests:

* interfaceConfig.json contains an overall interface configuration, including several interface instance
  configurations and rule configurations. It can be used to create an InterfaceConfig object
* updateInterfaceSetting.json contains a single interface instance configuration which can be used to
  create an interface specific ExtensionInterfaceConfig object
* updateRule.json contains a single rule configuration that can be used to create an interface
  specific ExtentionInterfaceRuleConfig object

send_test_jms_message.rb is a script to simulate interface interaction with the platform for testing

The exampleConfigs directory will be included in the initial repository.  If used in unit tests they
should be moved to somewhere under the tests directory.  If not needed, they can be deleted.
